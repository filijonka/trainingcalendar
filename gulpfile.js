var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var copy = copy = require('gulp-copy');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('dev-sass', function() {
    return gulp.src('./assets/sass/style.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(concat('bootstrap.css'))
        .pipe(gulp.dest('public/css'));
});

gulp.task('sass', function() {
    return gulp.src('./assets/sass/style.scss')
        .pipe(sass())
        .pipe(concat('bootstrap.css'))
        .pipe(gulp.dest('public/css'));
});

gulp.task('jquery', function() {

    gulp.src([
        'vendor/twbs/bootstrap/dist/js/bootstrap.js'
    ]).pipe(concat('bootstrap.js')).pipe(gulp.dest('public/js'));

    return gulp.src([
        './assets/scripts/letitrock.js'
    ])
    .pipe(gulp.dest('public/js'));

});

gulp.task('default', gulp.series('sass', 'jquery'));